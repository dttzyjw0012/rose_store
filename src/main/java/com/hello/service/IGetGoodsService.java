package com.hello.service;

import com.hello.service.impl.impl_classes.GoodsInfo;
import com.hello.service.impl.impl_classes.RecommendGoods;
import com.hello.service.impl.impl_classes.SearchGoods;

public interface IGetGoodsService
{
    RecommendGoods[] getGoodsByRecommendsType (String type, int num, int times);

    SearchGoods[] getGoodsBySearchType (String type, int num, int times, String key);

    GoodsInfo getGoodsInfo (Long id);
}
