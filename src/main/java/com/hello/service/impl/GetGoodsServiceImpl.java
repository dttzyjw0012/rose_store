package com.hello.service.impl;

import com.hello.dao.GoodsMapper;
import com.hello.model.Goods;
import com.hello.service.IGetGoodsService;
import com.hello.service.impl.impl_classes.GoodsInfo;
import com.hello.service.impl.impl_classes.RecommendGoods;
import com.hello.service.impl.impl_classes.SearchGoods;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service ("getGoodsService")
public class GetGoodsServiceImpl implements IGetGoodsService
{
    @Resource
    GoodsMapper GM;

    @Override
    public RecommendGoods[] getGoodsByRecommendsType (String type, int num, int times)
    {
        final String zong1He4 = "zhh";
        final String xiao1Liang4 = "xl";
        final String xin1ping3 = "xp";
        Goods[] goods;
        RecommendGoods[] recommendGoods;

        if (type != null)
        {
            if (type.compareTo(zong1He4) == 0)
                goods = GM.selectByRandomInFixedAmount(num);
            else if (type.compareTo(xiao1Liang4) == 0)
                goods = GM.selectBySalesInFixedAmount((times - 1) * num, num);
            else
                goods = GM.selectGoodsInFixedAmount((times - 1) * num, num, "publication_date",
                        "DESC");

            recommendGoods = new RecommendGoods[goods.length];
            for (int index = 0; index < goods.length; index++)
            {
                recommendGoods[index] = new RecommendGoods(goods[index].getId(), goods[index].getPrice(),
                        goods[index].getName(), goods[index].getInventoryQuantity(),
                        GM.searchSalesById(goods[index].getId()));
            }
        }
        else
        {
            recommendGoods = new RecommendGoods[1];
            recommendGoods[0] = new RecommendGoods();
        }

        return  recommendGoods;
    }

    @Override
    public SearchGoods[] getGoodsBySearchType (String type, int num, int times, String key)
    {
        final String zong1He4 = "zhh";
        final String xiao1Liang4 = "xl";
        final String xin1ping3 = "xp";
        final String jia4ge2sheng1 = "jgsh";
        final String jia4ge2jiang4 = "jgj";

        key = "%" + key + "%";

        Goods[] goods;
        SearchGoods[] searchGoods;

        if (type != null)
        {
            if (type.compareTo(zong1He4) == 0)
                goods = GM.searchGoodsByRandomInFixedAmount(num, key);
            else if (type.compareTo(xiao1Liang4) == 0)
                goods = GM.searchGoodsBySalesInFixedAmount((times - 1) * num, num, key);
            else if (type.compareTo(xin1ping3) == 0)
                goods = GM.searchGoodsInFixedAmount((times - 1) * num, num, "publication_date",
                        "DESC", key);
            else if (type.compareTo(jia4ge2sheng1) == 0)
                goods = GM.searchGoodsInFixedAmount((times - 1) * num, num, "price", "ASC", key);
            else
                goods = GM.searchGoodsInFixedAmount((times - 1) * num, num, "price", "DESC", key);

            searchGoods = new SearchGoods[goods.length];
            for (int index = 0; index < goods.length; index++)
            {
                searchGoods[index] = new SearchGoods(goods[index].getId(), goods[index].getPrice(), goods[index].getName(), goods[index].getInventoryQuantity());
            }
        }
        else
        {
            searchGoods = new SearchGoods[1];
            searchGoods[0] = new SearchGoods();
        }

        return  searchGoods;
    }

    @Override
    public GoodsInfo getGoodsInfo (Long id)
    {
        GoodsInfo GI;
        if (id != null)
        {
            Goods goods = GM.selectByPrimaryKey(id);

            GI = new GoodsInfo(goods.getId(), goods.getPrice(), goods.getName(), goods.getColors(), goods.getSizes(), goods.getInventoryQuantity(), goods.getShopId());
            return GI;
        }
        else
        {
            GI = new GoodsInfo();
            return GI;
        }
    }
}
