package com.hello.service;

import com.hello.model.GoodsSubtype;
import com.hello.model.GoodsType;

public interface IGetGoodsTypeService
{
    GoodsType[] selectAllGoodsTypes ();

    GoodsSubtype[] selectGoodsSubtypeById (Long id);
}
