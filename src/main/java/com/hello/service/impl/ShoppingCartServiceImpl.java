package com.hello.service.impl;

import com.hello.dao.BuyerMapper;
import com.hello.dao.ShoppingCartMapper;
import com.hello.model.ShoppingCart;
import com.hello.service.IShoppingCartService;
import com.hello.service.impl.impl_classes.AddShoppingCart;
import com.hello.service.impl.impl_classes.ShoppingCartDatas.Color;
import com.hello.service.impl.impl_classes.ShoppingCartDatas.Quantity;
import com.hello.service.impl.impl_classes.ShoppingCartDatas.Size;
import com.hello.service.impl.impl_classes.ShowShoppingCart;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service ("shoppingCartService")
public class ShoppingCartServiceImpl implements IShoppingCartService
{
    @Resource
    ShoppingCartMapper SCM;

    @Resource
    BuyerMapper BM;

    @Override
    public void addShoppingCart (AddShoppingCart addShoppingCart)
    {
        ShoppingCart shoppingCart;
        if ((shoppingCart = SCM.selectByBuyerId(BM.selectByAccount(addShoppingCart.getbuyerName()).getId())) == null)
        {
            shoppingCart = addShoppingCart.append();
            SCM.insert(shoppingCart);
        }
        else
        {
            addShoppingCart.append(shoppingCart);
            SCM.updateByPrimaryKey(shoppingCart);
        }
    }

    @Override
    public ShowShoppingCart showShoppingCart (String buyerName)
    {
        ShoppingCart shoppingCart;
        if ((shoppingCart = SCM.selectByBuyerId(BM.selectByAccount(buyerName).getId())) == null)
        {
            long buyerId = BM.selectByAccount(buyerName).getId();
            shoppingCart = new ShoppingCart(buyerId, buyerId);
            SCM.insert(shoppingCart);
        }

        ShowShoppingCart result = new ShowShoppingCart();
        result = new ShowShoppingCart(shoppingCart.getBuyerId(),
                result.getLongArrFromString(shoppingCart.getGoodsList()),
                (Quantity[]) result.getArrFromString(shoppingCart.getGoodsQuantityList(), "quantity"),
                (Color[]) result.getArrFromString(shoppingCart.getGoodsColorList(), "color"),
                (Size[]) result.getArrFromString(shoppingCart.getGoodsSizeList(), "size"));

        return result;
    }
}
