package com.hello.model;

import java.util.Date;

public class Buyer {
    private Long id;

    private String account;

    private String password;

    private String email;

    private Date registerDate;

    private String shippingAddress1;

    private String shippingAddress2;

    public Buyer()
    {

    }

    public Buyer (java.math.BigInteger id, String account, String password, String email, java.sql.Timestamp registerDate, String shippingAddress1, String shippingAddress2)
    {
        this.id = id.longValue();
        this.account = account;
        this.password = password;
        this.email = email;
        this.registerDate = registerDate;
        this.shippingAddress1 = shippingAddress1;
        this.shippingAddress2 = shippingAddress2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(String shippingAddress1) {
        this.shippingAddress1 = shippingAddress1 == null ? null : shippingAddress1.trim();
    }

    public String getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(String shippingAddress2) {
        this.shippingAddress2 = shippingAddress2 == null ? null : shippingAddress2.trim();
    }
}