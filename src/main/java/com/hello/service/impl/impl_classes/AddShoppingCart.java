package com.hello.service.impl.impl_classes;

import com.hello.dao.BuyerMapper;
import com.hello.model.ShoppingCart;

import javax.annotation.Resource;

public class AddShoppingCart
{
    private String buyerName;

    private long goodsId;

    private String[] goodsQuantityList;

    private String[] goodsColorList;

    private String[] goodsSizeList;

    @Resource
    BuyerMapper BM;

    public AddShoppingCart ()
    {

    }

    public AddShoppingCart (String buyerName, long goodsId, String[] goodsQuantityList, String[] goodsColorList, String[] goodsSizeList)
    {
        this.buyerName = buyerName;
        this.goodsId = goodsId;
        this.goodsQuantityList = goodsQuantityList;
        this.goodsColorList = goodsColorList;
        this.goodsSizeList = goodsSizeList;
    }

    public String getbuyerName ()
    {
        return buyerName;
    }

    public void setbuyerName (String buyerName)
    {
        this.buyerName = buyerName;
    }

    public long getGoodsId ()
    {
        return goodsId;
    }

    public void setGoodsId (long goodsId)
    {
        this.goodsId = goodsId;
    }

    public String[] getGoodsQuantityList ()
    {
        return goodsQuantityList;
    }

    public void setGoodsQuantityList (String[] goodsQuantityList)
    {
        this.goodsQuantityList = goodsQuantityList;
    }

    public String[] getGoodsColorList ()
    {
        return goodsColorList;
    }

    public void setGoodsColorList (String[] goodsColorList)
    {
        this.goodsColorList = goodsColorList;
    }

    public String[] getGoodsSizeList ()
    {
        return goodsSizeList;
    }

    public void setGoodsSizeList (String[] goodsSizeList)
    {
        this.goodsSizeList = goodsSizeList;
    }

    public ShoppingCart append ()
    {
        long id = BM.selectByAccount(buyerName).getId();

        ShoppingCart shoppingCart = new ShoppingCart(id, id, new Long(this.goodsId).toString(),
                getStringFromArr(this.goodsQuantityList), getStringFromArr(this.goodsColorList),
                getStringFromArr(this.goodsSizeList));

        return shoppingCart;
    }

    public ShoppingCart append (ShoppingCart shoppingCart)
    {
        shoppingCart.setGoodsList(shoppingCart.getGoodsList() + this.goodsId + " ");
        shoppingCart.setGoodsColorList(shoppingCart.getGoodsColorList() +
                getStringFromArr(this.goodsColorList) + " ");
        shoppingCart.setGoodsQuantityList(shoppingCart.getGoodsQuantityList() +
                getStringFromArr(this.goodsQuantityList) + " ");
        shoppingCart.setGoodsSizeList(shoppingCart.getGoodsSizeList() +
                getStringFromArr(this.goodsSizeList) + " ");

        return shoppingCart;
    }

    public static String getStringFromArr (String[] arr)
    {
        String result = "";

        for (String subString : arr)
        {
            result += subString;
            result += " ";
        }
        return result;
    }
}
