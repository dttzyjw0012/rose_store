package com.hello.service;

import com.hello.service.impl.impl_classes.BuyerId;

public interface ISelectBuyerIdService
{
    BuyerId selectBuyerIdByName (String account);
}
