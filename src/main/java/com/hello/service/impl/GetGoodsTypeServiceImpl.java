package com.hello.service.impl;

import com.hello.dao.GoodsSubtypeMapper;
import com.hello.model.GoodsSubtype;
import com.hello.model.GoodsType;
import com.hello.dao.GoodsTypeMapper;
import com.hello.service.IGetGoodsTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service ("getGoodsTypeService")
public class GetGoodsTypeServiceImpl implements IGetGoodsTypeService
{
    @Resource
    GoodsTypeMapper GTM;

    @Override
    public GoodsType[] selectAllGoodsTypes ()
    {
        int goodsTypeNum = GTM.getGoodsTypeNum();
        GoodsType[] GTs;
        if (goodsTypeNum != 0)
        {
            GTs = new GoodsType[goodsTypeNum];

            for (byte id = 0; id < goodsTypeNum; id++)
            {
                GTs[id] = GTM.selectByPrimaryKey(id);
            }
        }
        else
        {
            GTs = new GoodsType[1];
            GTs[0] = new GoodsType();
        }

        return GTs;
    }

    @Resource
    GoodsSubtypeMapper GSM;

    @Override
    public GoodsSubtype[] selectGoodsSubtypeById (Long id)
    {
        if (id != null)
            return GSM.selectByForeignKey(id);
        else
        {
            GoodsSubtype[] goodsSubtypes = new GoodsSubtype[1];
            goodsSubtypes[0] = new GoodsSubtype();
            return goodsSubtypes;
        }
    }
}
