package com.hello.service.impl.impl_classes.ShoppingCartDatas;

public class Color extends ShoppingCartData
{
    private String color;

    public String getColor ()
    {
        return color;
    }

    public void setColor (String color)
    {
        this.color = color;
    }

    public void setValue (String color)
    {
        this.color = color;
    }

    public static Color[] genArr(int len)
    {
        Color[] arr = new Color[len];

        for (int index = 0; index < len; index++)
            arr[index] = new Color();
        return arr;
    }
}
