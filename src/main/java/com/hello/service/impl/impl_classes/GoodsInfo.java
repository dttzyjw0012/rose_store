package com.hello.service.impl.impl_classes;

import java.util.Date;

public class GoodsInfo
{
    private Long id;

    private Float price;

    private String name;

    private String colors;

    private String sizes;

    private Integer inventoryQuantity;

    private Integer shopId;

    public GoodsInfo ()
    {

    }

    public GoodsInfo (Long id, Float price, String name, String colors, String sizes, Integer inventoryQuantity, Integer shopId)
    {
        this.id = id;
        this.price = price;
        this.name = name;
        this.colors = colors;
        this.sizes = sizes;
        this.inventoryQuantity = inventoryQuantity;
        this.shopId = shopId;
    }

    public Long getId ()
    {
        return id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }

    public Float getPrice ()
    {
        return price;
    }

    public void setPrice (Float price)
    {
        this.price = price;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getColors ()
    {
        return colors;
    }

    public void setColors (String colors)
    {
        this.colors = colors;
    }

    public String getSizes ()
    {
        return sizes;
    }

    public void setSizes (String sizes)
    {
        this.sizes = sizes;
    }

    public Integer getInventoryQuantity ()
    {
        return inventoryQuantity;
    }

    public void setInventoryQuantity (Integer inventoryQuantity)
    {
        this.inventoryQuantity = inventoryQuantity;
    }

    public Integer getShopId ()
    {
        return shopId;
    }

    public void setShopId (Integer shopId)
    {
        this.shopId = shopId;
    }
}
