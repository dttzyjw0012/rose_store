package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.service.IGetGoodsService;
import com.hello.service.impl.impl_classes.SearchGoods;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/Goods/")
public class SearchGoodsController
{
    @Resource
    private IGetGoodsService getGoodsService;

    @RequestMapping ("/getSearchGoods.json")
    public void register (HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String type = request.getParameter("type");
        int amount = Integer.parseInt(request.getParameter("num"));
        int times = Integer.parseInt(request.getParameter("times"));
        String key = request.getParameter("key");
        SearchGoods[] searchGoods = this.getGoodsService.getGoodsBySearchType(type, amount, times, key);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(searchGoods));
        response.getWriter().close();
    }
}
