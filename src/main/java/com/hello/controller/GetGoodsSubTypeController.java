package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.model.GoodsSubtype;
import com.hello.service.IGetGoodsTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/GoodsSubtype") public class GetGoodsSubTypeController
{
    @Resource
    private IGetGoodsTypeService getGoodsTypeService;

    @RequestMapping ("/showGoodsSubtypes.json")
    public void selectGoodsSubtypesBy (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        Long goodsTypeId = Long.parseLong(request.getParameter("type"));
        GoodsSubtype[] goodsSubtypes;
        goodsSubtypes = this.getGoodsTypeService.selectGoodsSubtypeById(goodsTypeId);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(goodsSubtypes));
        response.getWriter().close();
    }
}
