/*
    进行前端验证，如果有一个文本框为空，则不能提交
*/
function cheack() {
    var username = document.getElementById("username").value;   //获取用户名
    var password = document.getElementById("psw").value;    //获取用户密码
    var repassword = document.getElementById("repsw").value;    //获取用户输入的确认密码
    var email = document.getElementById("email").value;     //获取用户输入的邮箱号
    if (username == "") {
        alert("用户名不能为空");
        return;
    }
    if (password == "") {
        alert("密码不能为空");
        return;
    }
    if (repassword == "") {
        alert("请确认密码");
        return;
    }
    if (email == "") {
        alert("邮箱不能为空");
        return;
    }
    if (password != repassword) {
        alert("两次输入的密码不一样!");
        return;
    }
    loadXMLDoc(username, password, email);
}

function loadXMLDoc(username, password, email) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        // IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            //console.log(data);
            if (data.success) {
                //注册成功跳转到登录页面
                setTimeout("window.location.href ='login.html'", 2000);
            } else {
                alert("注册失败!");
                location.reload(true);  //刷新页面，重新输入
            }
        }
    }
    xmlhttp.open("POST", "res/Buyers/regSuccess.json", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("name=" + username + "&password=" + password + "&email=" + email);
}