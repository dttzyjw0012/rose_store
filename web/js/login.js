function cheackdata() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("psw").value;
    if (username == "") {
        alert("请输入用户名");
        return;
    }
    if (password == "") {
        alert("请输入密码");
        return;
    }
    loadXMLDoc(username, password);
}

function loadXMLDoc(u_name, password) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        // IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            if (data.success) {
                setCookie('username', u_name);
                setTimeout("location.href ='myself.html'", 1000); //1s之后跳转到我的主页
                //window.location.href ="myself.html";
            } else {
                alert("您的用户名或者密码不正确!");
                location.reload(true); //刷新页面
            }
        }
    }
    xmlhttp.open("POST", "res/Buyers/loginSuccess.json", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("name=" + u_name + "&password=" + password);
}
/*
将用户名存到Cookie中
*/
function setCookie(c_name, value) {
    //document.cookie = c_name + "=" + escape(value) +"; path=/";
    var exp = new Date();
    exp.setTime(exp.getTime() + 60 * 60 * 1000);
    document.cookie = c_name + "=" + escape(value) + ";expires=" + exp.toGMTString() + ";path=/";
}