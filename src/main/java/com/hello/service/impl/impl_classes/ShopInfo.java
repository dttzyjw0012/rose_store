package com.hello.service.impl.impl_classes;

import java.math.BigDecimal;

public class ShopInfo
{
    private BigDecimal descriptionMatchIndex;

    private BigDecimal qualitySatisfactionIndex;

    private Long favorQuantity;

    private String name;

    private Long sumSales;

    public ShopInfo ()
    {

    }

    public ShopInfo (BigDecimal descriptionMatchIndex, BigDecimal qualitySatisfactionIndex, String name,
                     Long sumSales)
    {
        this.descriptionMatchIndex = descriptionMatchIndex;
        this.qualitySatisfactionIndex = qualitySatisfactionIndex;
        this.favorQuantity = 0L;
        this.name = name;
        this.sumSales = sumSales;
    }

    public BigDecimal getDescriptionMatchIndex ()
    {
        return descriptionMatchIndex;
    }

    public void setDescriptionMatchIndex (BigDecimal descriptionMatchIndex)
    {
        this.descriptionMatchIndex = descriptionMatchIndex;
    }

    public BigDecimal getQualitySatisfactionIndex ()
    {
        return qualitySatisfactionIndex;
    }

    public void setQualitySatisfactionIndex (BigDecimal qualitySatisfactionIndex)
    {
        this.qualitySatisfactionIndex = qualitySatisfactionIndex;
    }

    public Long getFavorQuantity ()
    {
        return favorQuantity;
    }

    public void setFavorQuantity (Long favorQuantity)
    {
        this.favorQuantity = favorQuantity;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Long getSumSales ()
    {
        return sumSales;
    }

    public void setSumSales (Long sumSales)
    {
        this.sumSales = sumSales;
    }
}
