package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.service.IRegisterService;
import com.hello.service.impl.impl_classes.RecommendGoods;
import com.hello.service.impl.impl_classes.Success;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/Buyers/") public class RegistController
{
    @Resource
    private IRegisterService registerService;

    @RequestMapping ("/regSuccess.json")
    public void regist (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String registerAccount = request.getParameter("name");
        String registerPassword = request.getParameter("password");
        String registerEmail = request.getParameter("email");
        Success registerSuccess;
        registerSuccess = this.registerService.registerBuyer(registerAccount, registerPassword, registerEmail);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(registerSuccess));
        response.getWriter().close();
    }
}
