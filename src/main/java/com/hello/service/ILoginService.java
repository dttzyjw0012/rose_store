package com.hello.service;

import com.hello.service.impl.impl_classes.Success;

public interface ILoginService
{
    Success loginSuccess (String account, String password);
}
