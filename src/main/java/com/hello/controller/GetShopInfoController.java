package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.service.IGetShopInfoService;
import com.hello.service.impl.impl_classes.ShopInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/Shops/")
public class GetShopInfoController
{
    @Resource
    private IGetShopInfoService getShopInfoService;

    @RequestMapping ("/getShop.json")
    public void getShopInfo (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        Integer shopId = Integer.parseInt(request.getParameter("id"));
        ShopInfo shopInfo;
        shopInfo = this.getShopInfoService.getShopInfo(shopId);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(shopInfo));
        response.getWriter().close();
    }
}