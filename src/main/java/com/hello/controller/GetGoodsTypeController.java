package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.model.GoodsType;
import com.hello.service.IGetGoodsTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/GoodsType") public class GetGoodsTypeController
{
    @Resource
    private IGetGoodsTypeService goodsTypeService;

    @RequestMapping ("/showAllGoodsTypes.json")
    public void selectAllGoodsTypes (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        response.setCharacterEncoding("UTF-8");
        GoodsType[] allGoodsType = this.goodsTypeService.selectAllGoodsTypes();
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(allGoodsType));
        response.getWriter().close();
    }
}
