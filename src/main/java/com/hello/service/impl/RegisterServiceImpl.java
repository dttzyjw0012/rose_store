package com.hello.service.impl;

import com.hello.dao.BuyerMapper;
import com.hello.model.Buyer;
import com.hello.service.IRegisterService;
import com.hello.service.impl.impl_classes.Success;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Arrays;

@Service ("registerService")
public class RegisterServiceImpl implements IRegisterService
{
    @Resource
    BuyerMapper buyerMapper;

    @Override
    public Success registerBuyer (String name, String password, String email)
    {
        Success result = new Success();
        if (name == null || password == null || email == null)
        {
            result.setSuccess(false);
            return result;
        }
        else
        {
            Buyer buyerTest = buyerMapper.selectByAccount(name);
            if (buyerMapper.selectByAccount(name) != null)
            {
                result.setSuccess(false);
                return result;
            }
            else
            {

                Buyer buyer = new Buyer (new java.math.BigInteger(minIdNotInBuyers().toString()), name, password,
                        email, new Timestamp(System.currentTimeMillis()), "",
                        "");
                buyerMapper.insertSelective(buyer);
                result.setSuccess(true);
                return result;
            }
        }
    }

    Long minIdNotInBuyers ()
    {
        Long[] idAllExist = buyerMapper.selectAllId();
        long minId = 0;
        Arrays.sort(idAllExist);

        for (minId = 0; minId <= 2147483648L && minId < idAllExist.length; minId++)
        {
            if (minId != idAllExist[(int) minId])
                break;
        }

        return minId;
    }
}
