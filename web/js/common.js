//页面跳转触发事件 
function change(page) { //page为字符串,调用时页面用引号引起来
    window.location.href = page;
}
//用户搜索关键词时获取用户输入
function getData() {
    //得到页面参数
    var num = 20;
    var times = 1;
    var key = $("#search").val();
    var xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {}
    }
    //xmlhttp.open("POST","res/Goods/getRecommends.json",true);
    //xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.open("GET", "res/goods/searchGoods.json?num=" + num + "&times=" + times + "&key=" + key, true);
    xmlhttp.send();
}

