package com.hello.controller;

import com.hello.service.impl.GenOrderServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/Goods/")
public class GenOrderController
{
    @Resource
    GenOrderServiceImpl GOS;

    @RequestMapping ("/buyGoods.json")
    public void orderGenerate (HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        String buyerName = request.getParameter("buyerName");
        Long goodsId = Long.parseLong(request.getParameter("goodsId"));
        String color = request.getParameter("color");
        String size = request.getParameter("size");
        Integer num = Integer.parseInt(request.getParameter("num"));
        this.GOS.genOrder(buyerName, goodsId, color, size, num);
    }
}
