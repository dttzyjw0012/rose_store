package com.hello.service.impl;

import com.hello.dao.GoodsMapper;
import com.hello.dao.ShopMapper;
import com.hello.model.Shop;
import com.hello.service.IGetShopInfoService;
import com.hello.service.impl.impl_classes.ShopInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service ("getShopInfoService")
public class GetShopInfoService implements IGetShopInfoService
{
    @Resource
    ShopMapper SM;

    @Resource
    GoodsMapper GM;

    @Override
    public ShopInfo getShopInfo (Integer id)
    {
        ShopInfo SI;
        if (id != null && SM.selectByPrimaryKey(id) != null)
        {
            Shop shop = SM.selectByPrimaryKey(id);

            SI = new ShopInfo(shop.getDescriptionMatchIndex(), shop.getQualitySatisfactionIndex(),
                shop.getName(), getSumSales(id));
            return SI;
        }
        else
        {
            SI = new ShopInfo();
            return SI;
        }
    }

    private long getSumSales (int id)
    {
        long sumSales = 0;

        Long[] goodsIds = GM.selectIdByShopId(id);
        for (long goodsId : goodsIds)
            sumSales += GM.searchSalesById(goodsId);
        return sumSales;
    }
}
