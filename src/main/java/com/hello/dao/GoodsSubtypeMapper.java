package com.hello.dao;

import com.hello.model.GoodsSubtype;

public interface GoodsSubtypeMapper {
    int deleteByPrimaryKey (Short id);

    int insert (GoodsSubtype record);

    int insertSelective (GoodsSubtype record);

    GoodsSubtype selectByPrimaryKey (Short id);

    GoodsSubtype[] selectByForeignKey (Long id);

    int updateByPrimaryKeySelective (GoodsSubtype record);

    int updateByPrimaryKey (GoodsSubtype record);
}