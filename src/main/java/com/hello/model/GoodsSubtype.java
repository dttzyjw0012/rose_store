package com.hello.model;

public class GoodsSubtype {
    private Short id;

    private Byte goodsTypeId;

    private String goodsSubtypeName;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public Byte getGoodsTypeId() {
        return goodsTypeId;
    }

    public void setGoodsTypeId(Byte goodsTypeId) {
        this.goodsTypeId = goodsTypeId;
    }

    public String getGoodsSubtypeName() {
        return goodsSubtypeName;
    }

    public void setGoodsSubtypeName(String goodsSubtypeName) {
        this.goodsSubtypeName = goodsSubtypeName == null ? null : goodsSubtypeName.trim();
    }
}