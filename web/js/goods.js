$(document).ready(function() {
    var current_para = location.search.slice(1);
        var paras = current_para.split("&");
        var tp, get_key;
        for (var index = 0; index < paras.length; index++) {
            var paras1 = paras[index].slice(0, paras[index].indexOf("="));

            if (paras1 === "type") {
                tp = paras[index].slice(paras[index].indexOf("=") + 1);
            }
            if (paras1 === "key") {
                get_key = paras[index].slice(paras[index].indexOf("=") + 1);    //得到搜索词
                document.getElementById("title").innerHTML = decodeURI(get_key);    //标题栏显示搜索词
            }
        }  
    scroll(tp, get_key); 
})

/*
    点击按钮发送请求时截取地址栏中的类型和搜索词
*/
$(".button").click(function() {
        var current_para = location.search.slice(1);
        var paras = current_para.split("&");
        var tp, get_key;
        for (var index = 0; index < paras.length; index++) {
            var paras1 = paras[index].slice(0, paras[index].indexOf("="));
            if (paras1 === "type") {
                tp = paras[index].slice(paras[index].indexOf("=") + 1);
            }
            if (paras1 === "key") {
                get_key = paras[index].slice(paras[index].indexOf("=") + 1);
            }
        }  
    scroll(tp, get_key);  
});

/*
    底部无限滚动事件函数
        参数说明
            -ty  搜索的类型
            -key  搜索词
*/
function scroll(tp, key) {
    // 每次加载添加多少条目
    var itemsPerLoad = 1;
    //申请次数
    var times = 1;
     // 加载flag
    var loading = false;
    // 最多可加载的条目
    var maxItems = 120;
    //获取得到的json数据
    var datas = request_Data(tp, times, key);
    //此处加载文本内容
    //预先加载5条
    addItems(itemsPerLoad, 0, datas);
    // 上次加载的序号
    var lastIndex = 1;
    // 注册'infinite'事件处理函数
    $(document).on('infinite', function() {
        // 如果正在加载，则退出
        if (loading) return;
        // 设置flag
        loading = true;
        // 模拟1s的加载过程
        setTimeout(function() {
            // 重置加载flag
            loading = false;
            if (lastIndex >= maxItems) {
                // 加载完毕，则注销无限加载事件，以防不必要的加载
                $.detachInfiniteScroll($('.infinite-scroll'));
                // 删除加载提示符
                $('.infinite-scroll-preloader').remove();
                return;
            }
            datas = "";
            times++;
            datas = request_Data(tp, times, key);
            // 添加新条目
            addItems(itemsPerLoad, lastIndex, datas);
            // 更新最后加载的序号
            lastIndex = $('.list-container li').length;
            //容器发生改变,如果是js滚动，需要刷新滚动
            $.refreshScroller();
        }, 500);
    });
}

/*
    调用该函数返回的是总的后台数据
        参数说明
            -ty  搜索的类型
            -times  向后台申请的次数
            -keyNalue  搜索词
*/
function request_Data(tp, times, keyValue) {
    var xmlhttp;
    var data;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            data = JSON.parse(xmlhttp.responseText);
        }
    }
    xmlhttp.open("GET", "res/Goods/getSearchGoods.json?type=" + tp + "&num=10" + "&times=" + times + "&key=" + keyValue, false);
    xmlhttp.send();
    return data;
}

/*
    动态的创建返回的商品，每发一次请求页面中显示10条数据
        参数说明
            -number  预先加载的数据的个数，默认为1
            -lastIndex  上次加载的序号
            -datas  后台返回的数据集
*/
function addItems(number, lastIndex, datas) {
        for (var i = lastIndex + 1; i <= lastIndex + number; i++) {
            var li = document.createElement("li");
            var dv = document.createElement("div");
            dv.setAttribute("class", "row two");
            for (var j = 0; j < 10; j++) {
                var coldv = document.createElement("div");
                coldv.setAttribute("class", "col-50");
                var para = document.createElement("a");
                para.setAttribute("onclick", "change('details.html?id=" + datas[j].id + "')");
                coldv.appendChild(para);
                var inpara = document.createElement("img");
                inpara.setAttribute("src", "images/list/" + j + ".jpg"); //在此处添加要显示的图片编号
                para.appendChild(inpara);
                // para = document.createElement("span");
                // para.setAttribute("class", "sp");
                // node = document.createTextNode("已售" + datas[j].sales + "件");
                // para.appendChild(node);
                // coldv.appendChild(para);
                para = document.createElement("p");
                var node = document.createTextNode(datas[j].name);
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("span");
                para.setAttribute("class", "fl");
                node = document.createTextNode("￥" + datas[j].price);
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("span");
                para.setAttribute("class", "fr");
                node = document.createTextNode(datas[j].favorQuantity);
                para.appendChild(node);
                coldv.appendChild(para);
                var inpara = document.createElement("span");
                inpara.setAttribute("class", "icon icon-star");
                para.appendChild(inpara);
                dv.appendChild(coldv);
               
            }
            li.appendChild(dv);
            var ele = document.getElementById("list");
            ele.appendChild(li);      
        }
}


