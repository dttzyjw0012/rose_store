package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.service.ILoginService;
import com.hello.service.impl.impl_classes.Success;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/Buyers") public class LoginController
{
    @Resource
    private ILoginService loginService;

    @RequestMapping ("/loginSuccess.json")
    public void login (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String account = request.getParameter("name");
        String password = request.getParameter("password");
        Success loginSuccess = this.loginService.loginSuccess(account, password);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(loginSuccess));
        response.getWriter().close();
    }
}
