package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.service.IGetGoodsService;
import com.hello.service.impl.impl_classes.GoodsInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/Goods/")
public class GetGoodsInfoController
{
    @Resource
    private IGetGoodsService getGoodsService;

    @RequestMapping ("/getInfo.json")
    public void getGoodsInfo (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        Long goodsId = Long.parseLong(request.getParameter("id"));
        GoodsInfo goodsInfo;
        goodsInfo = this.getGoodsService.getGoodsInfo(goodsId);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(goodsInfo));
        response.getWriter().close();
    }
}
