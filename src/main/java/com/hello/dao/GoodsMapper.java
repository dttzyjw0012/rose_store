package com.hello.dao;

import com.hello.model.Goods;

public interface GoodsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);

    Goods[] selectByRandomInFixedAmount (int num);

    Goods[] selectBySalesInFixedAmount (int startIndex, int num);

    Goods[] selectGoodsInFixedAmount (int startIndex, int num, String type, String direct);

    Goods[] searchGoodsByRandomInFixedAmount(int num, String key);

    Goods[] searchGoodsBySalesInFixedAmount (int startIndex, int num, String key);

    Goods[] searchGoodsInFixedAmount (int startIndex, int num, String type, String direct, String key);

    int searchSalesById(Long id);

    Long[] selectIdByShopId(Integer shopId);

    Long[] selectAllId();
}
