package com.hello.dao;

import com.hello.model.GoodsType;

public interface GoodsTypeMapper {
    int deleteByPrimaryKey (Byte id);

    int insert (GoodsType record);

    int insertSelective (GoodsType record);

    GoodsType selectByPrimaryKey (Byte id);

    int updateByPrimaryKeySelective (GoodsType record);

    int updateByPrimaryKey (GoodsType record);

    int getGoodsTypeNum ();
}