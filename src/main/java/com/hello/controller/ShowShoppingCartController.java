package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.service.IShoppingCartService;
import com.hello.service.impl.impl_classes.ShowShoppingCart;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("res/ShoppingCarts/")
public class ShowShoppingCartController
{
    @Resource
    private IShoppingCartService shoppingCartService;

    @RequestMapping ("/showShoppingCart.json")
    public void showShoppingCart (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String buyerName = request.getParameter("buyerName");
        ShowShoppingCart showShoppingCart = this.shoppingCartService.showShoppingCart(buyerName);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(showShoppingCart));
        response.getWriter().close();
    }
}
