package com.hello.service.impl.impl_classes.ShoppingCartDatas;

public class Size extends ShoppingCartData
{
    private String size;

    public String getSize ()
    {
        return size;
    }

    public void setSize (String size)
    {
        this.size = size;
    }

    public void setValue (String size)
    {
        this.size = size;
    }

    public static Size[] genArr(int len)
    {
        Size[] arr = new Size[len];

        for (int index = 0; index < len; index++)
            arr[index] = new Size();
        return arr;
    }
}
