package com.hello.service.impl;

import com.hello.dao.BuyerMapper;
import com.hello.dao.GoodsMapper;
import com.hello.dao.OrderMapper;
import com.hello.model.Order;
import com.hello.service.IGenOrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;

@Service ("genOrderService")
public class GenOrderServiceImpl implements IGenOrderService
{
    @Resource
    OrderMapper OM;

    @Resource
    BuyerMapper BM;

    @Override
    public void genOrder (String buyerName, long goodsId, String color, String size, int num)
    {
        if (!(buyerName == null || buyerName.compareTo("") == 0 || goodsId == 0 || color == null
                || size == null || num == 0))
        {
            long buyerId = BM.selectByAccount(buyerName).getId();

            Order order = new Order(minIdNotInOrders(), new Timestamp(System.currentTimeMillis()),
                    new java.util.Date(), new java.util.Date(), new java.util.Date(), buyerId, goodsId,
                    "", "");
            OM.insertSelective(order);
        }
    }

    Long minIdNotInOrders()
    {
        Long[] idAllExist = OM.selectAllId();
        long minId = 0;
        Arrays.sort(idAllExist);

        for (minId = 0; minId <= 2147483648L && minId < idAllExist.length; minId++)
        {
            if (minId != idAllExist[(int) minId])
                break;
        }

        return minId;
    }
}
