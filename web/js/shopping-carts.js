$(document).ready(function() {
    var username = getCookie('username');
    if (username != "") {
        getGoods(username);
    }
});

/*
    加载购物车中的商品，前提是用户已经登陆的情况下
        参数说明
            -buyername  用户名
*/
function getGoods(buyername) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            //var data = get_shopping_cart();
            var data = JSON.parse(xmlhttp.responseText);
            var title = document.getElementsByClassName("title");
            var goods_amount;
            if (data.goodsList[0] === null) goods_amount = 0;
            else goods_amount = data.goodsList.length;
            title[0].innerHTML = "购物车(" + goods_amount + ")";
            for (var goods_index = 0; goods_index < goods_amount; goods_index++) {
                var item_ul = document.getElementById("item-ul");
                var li_0 = document.createElement("li");
                var label = document.createElement("label");
                var input = document.createElement("input");
                var div_0 = document.createElement("div");
                var i = document.createElement("i");
                var div_1 = document.createElement("div");
                var div_2 = document.createElement("div");
                var div_3 = document.createElement("div");
                var ul = document.createElement("ul");
                var li_1 = document.createElement("li");
                var div_4 = document.createElement("div");
                var div_5 = document.createElement("div");
                var img = document.createElement("img");
                var div_6 = document.createElement("div");
                var div_7 = document.createElement("div");
                var div_8 = document.createElement("div");
                var div_9 = document.createElement("div");
                var div_10 = document.createElement("div");
                var div_11 = document.createElement("div");
                label.classList.add("label-checkbox");
                label.classList.add("item-content");
                input.type = "checkbox";
                input.name = "checkbox";
                div_0.classList.add("item-media");
                i.classList.add("icon");
                i.classList.add("icon-form-checkbox");
                div_1.classList.add("item-inner");
                div_2.classList.add("item-title-row");
                div_3.classList.add("list-block");
                div_3.classList.add("media-list");
                ul.style = "padding-left:0px";
                div_4.classList.add("item-content");
                div_5.classList.add("item-media");
                img.src = "images\\goods\\0\\logo.webp";
                img.style = "width: 4rem";
                div_6.classList.add("item-inner");
                div_7.classList.add("item-title-row");
                div_8.classList.add("item-text");
                div_8.innerHTML = get_goods_name(data.buyerId);
                div_9.classList.add("item-text");
                div_9.innerHTML = "颜色：" + data.goodsColorList[goods_index].color + "；尺码：" + data.goodsSizeList[goods_index].size;
                div_10.classList.add("item-title");
                div_10.style = "float: left; width:165px";
                div_10.innerHTML = "￥" + get_goods_price(data.goodsList[goods_index].goodsId);
                div_11.classList.add("item-amount");
                div_11.style = "font-size: 10px; display:table-cell; vertical-align:bottom; height: 28px";
                div_11.innerHTML = "×" + data.goodsQuantityList[goods_index].quantity;
                item_ul.appendChild(li_0);
                li_0.appendChild(label);
                label.appendChild(input);
                label.appendChild(div_0);
                div_0.appendChild(i);
                label.appendChild(div_1);
                div_1.appendChild(div_2);
                div_2.appendChild(div_3);
                div_3.appendChild(ul);
                ul.appendChild(li_1);
                li_1.appendChild(div_4);
                div_4.appendChild(div_5);
                div_5.appendChild(img);
                div_4.appendChild(div_6);
                div_6.appendChild(div_7);
                div_7.appendChild(div_8);
                div_6.appendChild(div_9);
                div_6.appendChild(div_10);
                div_6.appendChild(div_11);
            }
        }
    }
    xmlhttp.open("POST", "res/ShoppingCarts/showShoppingCart.json", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("buyerName=" + buyername);
}

/*
    加载商品的名字
        参数说明
            -goods_id  商品的id号
*/
function get_goods_name(goods_id) {
    let name;
    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            name = data.name
        }
    }
    xmlhttp.open("POST", "res/Goods/getInfo.json", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("id=" + goods_id);
    return name;
}

/*
    加载商品的价格
        参数说明
            -goods_id  商品的id号
*/
function get_goods_price(goods_id) {
    let xmlhttp;
    let price;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            price = data.price;
        }
    }
    xmlhttp.open("POST", "res/Goods/getInfo.json", false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("id=" + goods_id);
    return price;
}

/*
    检查用户名是否存在
*/
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}