package com.hello.model;

import java.util.Date;

public class Order {
    private Long id;

    private Date orderTime;

    private Date deliveryTime;

    private Date receivingTime;

    private Date closingTime;

    private Long buyerId;

    private Long goodsId;

    private String express;

    private String expressNumber;

    Order ()
    {

    }

    public Order (Long id, Date orderTime, Date deliveryTime, Date receivingTime, Date closingTime, Long buyerId, Long goodsId, String express, String expressNumber)
    {
        this.id = id;
        this.orderTime = orderTime;
        this.deliveryTime = deliveryTime;
        this.receivingTime = receivingTime;
        this.closingTime = closingTime;
        this.buyerId = buyerId;
        this.goodsId = goodsId;
        this.express = express;
        this.expressNumber = expressNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Date getReceivingTime() {
        return receivingTime;
    }

    public void setReceivingTime(Date receivingTime) {
        this.receivingTime = receivingTime;
    }

    public Date getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(Date closingTime) {
        this.closingTime = closingTime;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getExpress() {
        return express;
    }

    public void setExpress(String express) {
        this.express = express == null ? null : express.trim();
    }

    public String getExpressNumber() {
        return expressNumber;
    }

    public void setExpressNumber(String expressNumber) {
        this.expressNumber = expressNumber == null ? null : expressNumber.trim();
    }
}