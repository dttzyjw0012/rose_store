  $(document).ready(function() {
      var button = document.getElementById("pro_index");
      var div = button.parentNode;
      var botton_width = button.clientWidth || o.offsetWidth;;
      var div_width = div.clientWidth || o.offsetWidth;;
      var margin_left = (div_width - botton_width) / 2 - 2;
      button.style.marginLeft = margin_left + "px";
      carts_amount.innerHTML = get_carts_amount();
  });

/*
    get_carts_amount()函数通过用户名查询该用户购物车中商品的个数，返回商品数量
*/
  function get_carts_amount() {
      var xmlhttp;
      var goods_amount;
      if (window.XMLHttpRequest) {
          //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
          xmlhttp = new XMLHttpRequest();
      } else {
          // IE6, IE5 浏览器执行代码
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
          if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
              //var data = get_shopping_cart();
              var data = JSON.parse(xmlhttp.responseText);
              var title = document.getElementsByClassName("title");
              if (data.goodsList[0] === null) goods_amount = 0;
              else goods_amount = data.goodsList.length;
          }
      }
      xmlhttp.open("POST", "res/ShoppingCarts/showShoppingCart.json", false);
      xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xmlhttp.send("buyerName=" + getCookie('username'));
      return goods_amount;
  }

/*
    用户登录时将用户名写到浏览器缓存中
*/
  function setCookie(c_name, value) {
      //document.cookie = c_name + "=" + escape(value) +"; path=/";
      var exp = new Date();
      exp.setTime(exp.getTime() + 60 * 60 * 1000);
      document.cookie = c_name + "=" + escape(value) + ";expires=" + exp.toGMTString() + ";path=/";
  }

/*
    取到浏览器中缓存的用户名并返回
*/
  function getCookie(c_name) {
      if (document.cookie.length > 0) {
          c_start = document.cookie.indexOf(c_name + "=");
          if (c_start != -1) {
              c_start = c_start + c_name.length + 1;
              c_end = document.cookie.indexOf(";", c_start);
              if (c_end == -1) c_end = document.cookie.length;
              return unescape(document.cookie.substring(c_start, c_end));
          }
      }
      return "";
  }

/*
    文档加载时执行，检查浏览器缓存中是否有用户名，存在则显示用户名，不存在跳转到登录页面
*/
  function checkCookie() {
      var username = getCookie('username');
      if (username != null && username != "") {
          document.getElementById("p").innerHTML = username;
      } else {
          if (username == null || username == "") {
              setTimeout("location.href ='login.html'", 0);
          }
      }
  }

/*
    点击退出登录时触发，删除浏览器中缓存的用户信息
*/
  function delCookie() {
      setCookie('username', "");    //设置用户名为空
      setTimeout("location.href ='index.html'", 0); //退出登录后，跳转到主页
  }