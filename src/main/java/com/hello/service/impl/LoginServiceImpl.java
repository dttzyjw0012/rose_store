package com.hello.service.impl;

import com.hello.dao.BuyerMapper;
import com.hello.model.Buyer;
import com.hello.service.ILoginService;
import com.hello.service.impl.impl_classes.Success;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service ("loginService")
public class LoginServiceImpl implements ILoginService
{
    @Resource
    BuyerMapper BM;

    @Override
    public Success loginSuccess (String account, String password)
    {
        Buyer buyer;
        Success success = new Success();
        if (account != null && password != null)
        {
            if ((buyer = BM.selectByAccount(account)) != null && buyer.getPassword().compareTo(password) == 0)
            {
                success.setSuccess(true);
                return success;
            }
            else
            {
                success.setSuccess(false);
                return success;
            }
        }
        else
        {
            success.setSuccess(false);
            return success;
        }
    }
}
