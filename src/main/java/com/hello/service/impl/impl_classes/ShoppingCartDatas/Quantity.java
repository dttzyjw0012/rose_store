package com.hello.service.impl.impl_classes.ShoppingCartDatas;

public class Quantity extends ShoppingCartData
{
    private String quantity;

    public String getQuantity ()
    {
        return quantity;
    }

    public void setQuantity (String quantity)
    {
        this.quantity = quantity;
    }

    public void setValue (String quantity)
    {
        this.quantity = quantity;
    }

    public static Quantity[] genArr(int len)
    {
        Quantity[] arr = new Quantity[len];

        for (int index = 0; index < len; index++)
            arr[index] = new Quantity();
        return arr;
    }
}
