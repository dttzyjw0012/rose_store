package com.hello.controller;

import com.hello.service.IShoppingCartService;
import com.hello.service.impl.impl_classes.AddShoppingCart;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("res/ShoppingCarts/")
public class AddShoppingCartController
{
    @Resource
    private IShoppingCartService shoppingCartService;

    @RequestMapping ("/addShoppingCart.json")
    public void addShoppingCart (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String buyerName = request.getParameter("buyerName");
        Long goodsId = Long.parseLong(request.getParameter("goodsId"));
        String[] goodsQuantityList = request.getParameterValues("quantity");
        String[] goodsColorList = request.getParameterValues("color");
        String[] goodsSizeList = request.getParameterValues("size");
        AddShoppingCart addShoppingCart;
        addShoppingCart = new AddShoppingCart(buyerName, goodsId, goodsQuantityList, goodsColorList,
                goodsSizeList);
        this.shoppingCartService.addShoppingCart(addShoppingCart);
    }
}
