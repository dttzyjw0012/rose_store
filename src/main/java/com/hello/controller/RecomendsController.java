package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.service.IGetGoodsService;
import com.hello.service.impl.impl_classes.RecommendGoods;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/Goods")
public class RecomendsController
{
    @Resource
    private IGetGoodsService getGoodsService;

    @RequestMapping ("/getRecommends.json")
    public void recommend (HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String goodsRecommendsType = request.getParameter("type");
        int goodsRecommendsAmount = Integer.parseInt(request.getParameter("num"));
        int goodsRecommendstimes = Integer.parseInt(request.getParameter("times"));
        RecommendGoods[] recommendGoods;
        recommendGoods = this.getGoodsService.getGoodsByRecommendsType(goodsRecommendsType,
                goodsRecommendsAmount, goodsRecommendstimes);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(recommendGoods));
        response.getWriter().close();
    }
}
