package com.hello.model;

import java.math.BigDecimal;

public class Shop {
    private Integer id;

    private Byte rentPayStatus;

    private BigDecimal descriptionMatchIndex;

    private BigDecimal qualitySatisfactionIndex;

    private Long sellerId;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getRentPayStatus() {
        return rentPayStatus;
    }

    public void setRentPayStatus(Byte rentPayStatus) {
        this.rentPayStatus = rentPayStatus;
    }

    public BigDecimal getDescriptionMatchIndex() {
        return descriptionMatchIndex;
    }

    public void setDescriptionMatchIndex(BigDecimal descriptionMatchIndex) {
        this.descriptionMatchIndex = descriptionMatchIndex;
    }

    public BigDecimal getQualitySatisfactionIndex() {
        return qualitySatisfactionIndex;
    }

    public void setQualitySatisfactionIndex(BigDecimal qualitySatisfactionIndex) {
        this.qualitySatisfactionIndex = qualitySatisfactionIndex;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}