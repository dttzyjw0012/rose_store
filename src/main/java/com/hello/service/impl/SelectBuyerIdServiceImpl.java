package com.hello.service.impl;

import com.hello.dao.BuyerMapper;
import com.hello.model.Buyer;
import com.hello.service.ISelectBuyerIdService;
import com.hello.service.impl.impl_classes.BuyerId;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigInteger;

@Service ("selectBuyerIdService")
public class SelectBuyerIdServiceImpl implements ISelectBuyerIdService
{
    @Resource
    BuyerMapper buyerMapper;

    @Override
    public BuyerId selectBuyerIdByName (String account)
    {
        Buyer buyerSelected = buyerMapper.selectByAccount(account);

        if (buyerSelected == null)
        {
            return new BuyerId(-1L);
        }
        else
        {
            return new BuyerId(buyerSelected.getId());
        }
    }
}
