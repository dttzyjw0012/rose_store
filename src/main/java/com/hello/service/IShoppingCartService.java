package com.hello.service;

import com.hello.service.impl.impl_classes.AddShoppingCart;
import com.hello.service.impl.impl_classes.ShowShoppingCart;

public interface IShoppingCartService
{
    void addShoppingCart (AddShoppingCart addShoppingCart);

    ShowShoppingCart showShoppingCart (String buyerName);
}
