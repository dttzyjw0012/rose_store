package com.hello.service.impl.impl_classes;

public class BuyerId
{
    Long id;

    public BuyerId (Long id)
    {
        this.id = id;
    }

    public Long getId ()
    {
        return id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }
}
