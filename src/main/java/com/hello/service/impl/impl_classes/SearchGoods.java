package com.hello.service.impl.impl_classes;

public class SearchGoods
{
    private long id;
    private float price;
    private String name;
    private int favorQuantity;

    public SearchGoods ()
    {

    }

    public SearchGoods (long id, float price, String name, int favorQuantity)
    {
        this.id = id;
        this.price = price;
        this.name = name;
        this.favorQuantity = favorQuantity;
    }

    public long getId ()
    {
        return id;
    }

    public void setId (long id)
    {
        this.id = id;
    }

    public float getPrice ()
    {
        return price;
    }

    public void setPrice (float price)
    {
        this.price = price;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public int getFavorQuantity ()
    {
        return favorQuantity;
    }

    public void setFavorQuantity (int favorQuantity)
    {
        this.favorQuantity = favorQuantity;
    }
}
