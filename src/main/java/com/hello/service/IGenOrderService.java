package com.hello.service;

public interface IGenOrderService
{
    void genOrder (String buyerName, long goodsId, String color, String size, int num);
}
