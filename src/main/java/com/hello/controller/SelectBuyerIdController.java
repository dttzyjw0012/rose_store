package com.hello.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.hello.service.ISelectBuyerIdService;
import com.hello.service.impl.impl_classes.BuyerId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller @RequestMapping ("/res/Buyers/") public class SelectBuyerIdController
{
    @Resource
    private ISelectBuyerIdService selectBuyerIdService;

    @RequestMapping ("/getBuyerId.json")
    public void register (HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String account = request.getParameter("name");
        BuyerId id = this.selectBuyerIdService.selectBuyerIdByName(account);
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(id));
        response.getWriter().close();
    }
}
