/*
底部无限滚动事件函数
*/
//申请次数
var times = 0;
// 每次加载添加多少条目
var itemsPerLoad = 1;
$(function() {
    // 加载flag
    var loading = false;
    // 最多可加载的条目
    var maxItems = 120;

    /*加载猜你喜欢部分的内容
        参数说明
            -number  加载的数据条数
            -lastIndex  上次加载的序号
    */
    function addItems(number, lastIndex) {
        for (var i = lastIndex + 1; i <= lastIndex + number; i++) {
            getText();
        }
    }
    //预先加载1条
    addItems(itemsPerLoad, 0);
    // 上次加载的序号
    var lastIndex = 1;
    // 注册'infinite'事件处理函数
    $(document).on('infinite', function() {
        // 如果正在加载，则退出
        if (loading) return;
        // 设置flag
        loading = true;
        // 模拟1s的加载过程
        setTimeout(function() {
            // 重置加载flag
            loading = false;
            if (lastIndex >= maxItems) {
                // 加载完毕，则注销无限加载事件，以防不必要的加载
                $.detachInfiniteScroll($('.infinite-scroll'));
                // 删除加载提示符
                $('.infinite-scroll-preloader').remove();
                return;
            }
            // 添加新条目
            addItems(itemsPerLoad, lastIndex);
            // 更新最后加载的序号
            lastIndex = $('.list-container li').length;
            //容器发生改变,如果是js滚动，需要刷新滚动
            $.refreshScroller();
        }, 500);
        times++;
    });
})
var index = 0;

/*
    动态生成猜你喜欢部分的数据，每刷新一次生成10条
*/
function getText() {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            var li = document.createElement("li");
            var dv = document.createElement("div");
            dv.setAttribute("class", "row two");
            for (var j = 0; j < 10; j++) {
                var coldv = document.createElement("div");
                coldv.setAttribute("class", "col-50");
                var para = document.createElement("a");
                //para.setAttribute("onclick", "change('details.html')");
                para.setAttribute("onclick", "change('details.html?id=" + index + "')");
                coldv.appendChild(para);
                var inpara = document.createElement("img");
                inpara.setAttribute("src", "images/list/" + data[j].id + ".jpg"); //在此处添加要显示的图片编号
                para.appendChild(inpara);
                para = document.createElement("span");
                para.setAttribute("class", "sp");
                // para.setAttribute("style","height:20px;width:90px;margin-top:172px; margin-left:-153px;position:absolute;background:#AEACAC");
                node = document.createTextNode("已售" + data[j].sales + "件");
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("p");
                //para.setAttribute("style","margin:0px auto;height:25px;width:100%;text-align:center");
                var node = document.createTextNode(data[j].name);
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("span");
                para.setAttribute("class", "fl");
                //para.setAttribute("style","height:20px;width:50px;float:left;color:red");
                node = document.createTextNode("￥" + data[j].price);
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("span");
                para.setAttribute("class", "fr");
                //para.setAttribute("style","height:20px;width:50px;float:right");
                node = document.createTextNode(data[j].favorQuantity);
                para.appendChild(node);
                coldv.appendChild(para);
                var inpara = document.createElement("span");
                inpara.setAttribute("class", "icon icon-star");
                para.appendChild(inpara);
                dv.appendChild(coldv);
                index++;
            }
            li.appendChild(dv);
            var ele = document.getElementById("list");
            ele.appendChild(li);
        }
    }
    //xmlhttp.open("POST","res/Goods/getRecommends.json",true);
    //xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.open("GET", "res/Goods/getRecommends.json?type=zhh&num=10" + "&times=" + times, true);
    xmlhttp.send();
}