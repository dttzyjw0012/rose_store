package com.hello.model;

public class ShoppingCart {
    private Long id;

    private Long buyerId;

    private String goodsList;

    private String goodsQuantityList;

    private String goodsColorList;

    private String goodsSizeList;

    public ShoppingCart ()
    {

    }

    public ShoppingCart (Long id, Long buyerId)
    {
        this.id = id;
        this.buyerId = buyerId;
        this.goodsList = "";
        this.goodsQuantityList = "";
        this.goodsColorList = "";
        this.goodsSizeList = "";
    }

    public ShoppingCart (Long id, Long buyerId, String goodsList, String goodsQuantityList, String goodsColorList, String goodsSizeList)
    {
        this.id = id;
        this.buyerId = buyerId;
        this.goodsList = goodsList;
        this.goodsQuantityList = goodsQuantityList;
        this.goodsColorList = goodsColorList;
        this.goodsSizeList = goodsSizeList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public String getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(String goodsList) {
        this.goodsList = goodsList == null ? null : goodsList.trim();
    }

    public String getGoodsQuantityList() {
        return goodsQuantityList;
    }

    public void setGoodsQuantityList(String goodsQuantityList) {
        this.goodsQuantityList = goodsQuantityList == null ? null : goodsQuantityList.trim();
    }

    public String getGoodsColorList() {
        return goodsColorList;
    }

    public void setGoodsColorList(String goodsColorList) {
        this.goodsColorList = goodsColorList == null ? null : goodsColorList.trim();
    }

    public String getGoodsSizeList() {
        return goodsSizeList;
    }

    public void setGoodsSizeList(String goodsSizeList) {
        this.goodsSizeList = goodsSizeList == null ? null : goodsSizeList.trim();
    }
}