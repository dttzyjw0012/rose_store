$(document).ready(function() {
    let xmlhttp = public();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            var text = "";
            for (var i = 0; i < data.length; i++) {
                text += data[i].goodsTypeName;
                document.getElementById("li" + data[i].id).innerHTML = text;
                text = "";
            }
            loader(0);  //文档载入时默认显示的是第一个分类下的商品
        } else {
            var data = [{
                id: 0,
                goodsTypeName: "加载错误"
            }, {
                id: 1,
                goodsTypeName: "加载错误"
            }, {
                id: 2,
                goodsTypeName: "加载错误"
            }, {
                id: 3,
                goodsTypeName: "加载错误"
            }];
            var text = "";
            for (var i = 0; i < data.length; i++) {
                text += data[i].goodsTypeName;
                document.getElementById("li" + data[i].id).innerHTML = text;
                text = "";
            }
        }
    }
    xmlhttp.open("GET", "res/GoodsType/showAllGoodsTypes.json", true);
    xmlhttp.send();   

});
//获取li标签的下标，将下标值传递给loader()
$("li").click(function() {
    var index = $(this).index();
    loader(index);
});

/*
    
*/
function loader(ind) {
    loader1(ind);
    var str = location.search;
        type = str.indexOf('=') + 1;
        var tp = str.substr(type);
        console.log(tp);
        request_Data(tp);
}

/*
    动态创建商品的子分类区域
        参数说明
            -ind  用户点击左边菜单栏时的下标值
*/
function loader1(ind) {
    let xmlhttp = public();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var da = JSON.parse(xmlhttp.responseText);
            /*向col-80的div里边动态加入标签  start*/
            var index = 0;
            txt = "";
            for (var i = 0; i < da.length / 3; i++) {
                txt += "<div class=\"row\">";
                for (var j = 0; j < 3; j++) {
                    txt += "<div class=\"col-33\" id=\"" + "a" + index + "\"></div>";
                    index++;
                }
                txt += "</div>";
            }
            if (da.length % 3 > 0) {
                txt += "<div class=\"row\">";
                for (var index = 1; index <= da.length % 3; index++) txt += "<div class=\"col-33\" id=\"" + "a" + index + "\"></div>";
                txt += "</div>";
            }
            document.getElementById("top").innerHTML = txt;
            /*   end   */
            var text = "";
            for (var i = 0; i < da.length; i++) {
                text += "<a onclick=change('list.html?id=" + i + "&goodsName=" + escape(da[i].goodsSubtypeName) + "')" + ">" + "<img src=images/class/sub_class" + ind + "/" + i + ".jpg" + ">" + "<br /><span>" + da[i].goodsSubtypeName + "</span></a>";
                document.getElementById("a" + i).innerHTML = text;
                text = "";
            }
        } else {
            var da = [{
                id: 0,
                goodsTypeId: ind,
                goodsSubtypeName: "加载错误"
            }, {
                id: 1,
                goodsTypeId: ind,
                goodsSubtypeName: "加载错误"
            }, {
                id: 2,
                goodsTypeId: ind,
                goodsSubtypeName: "加载错误"
            }, {
                id: 3,
                goodsTypeId: ind,
                goodsSubtypeName: "加载错误"
            }, {
                id: 4,
                goodsTypeId: ind,
                goodsSubtypeName: "加载错误"
            }];
            var index = 0;
            txt = "";
            for (var i = 0; i < da.length / 3; i++) {
                txt += "<div class=\"row\">";
                for (var j = 0; j < 3; j++) {
                    txt += "<div class=\"col-33\" id=\"" + "a" + index + "\"></div>";
                    index++;
                }
                txt += "</div>";
            }
            if (da.length % 3 > 0) {
                txt += "<div class=\"row\">";
                for (var index = 1; index <= da.length % 3; index++) txt += "<div class=\"col-33\" id=\"" + "a" + index + "\"></div>";
                txt += "</div>";
            }
            document.getElementById("top").innerHTML = txt;
            var text = "";
            for (var i = 0; i < da.length; i++) {
                text += "<a href=" + "#" + ">" + "<img src=images/class/sub_class" + ind + "/" + da[i].id + ".jpg" + ">" + "<br /><span>" + da[i].goodsSubtypeName + "</span></a>";
                document.getElementById("a" + i).innerHTML = text;
                text = "";
            }
        }
    }
    xmlhttp.open("POST", "res/GoodsSubtype/showGoodsSubtypes.json", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("type=" + ind);
}

/*
    获取并返回xmlhttp对象
*/
function public() {
    let xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        return xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        return xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
}

$(".click-btn").click(function() {
    
});

/*
    将请求到的数据动态添加到商品推荐区域
        参数说明
          -tp  请求的商品的类型
*/
function request_Data(tp) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var datas = JSON.parse(xmlhttp.responseText);
            var li = document.createElement("li");
            var dv = document.createElement("div");
            dv.setAttribute("class", "row two");
            for (var j = 0; j < 20; j++) {
                var coldv = document.createElement("div");
                coldv.setAttribute("class", "col-50");
                var para = document.createElement("a");
                para.setAttribute("onclick", "change('details.html?id=" + datas[j].id + "')");
                coldv.appendChild(para);
                var inpara = document.createElement("img");
                inpara.setAttribute("src", "images/list/" + j + ".jpg"); //在此处添加要显示的图片编号
                para.appendChild(inpara);
                para = document.createElement("span");
                para.setAttribute("class", "sp");
                node = document.createTextNode("已售" + datas[j].sales + "件");
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("p");
                var node = document.createTextNode(datas[j].name);
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("span");
                para.setAttribute("class", "fl");
                node = document.createTextNode("￥" + datas[j].price);
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("span");
                para.setAttribute("class", "fr");
                node = document.createTextNode(datas[j].favorQuantity);
                para.appendChild(node);
                coldv.appendChild(para);
                var inpara = document.createElement("span");
                inpara.setAttribute("class", "icon icon-star");
                para.appendChild(inpara);
                dv.appendChild(coldv);
            }
            li.appendChild(dv);
            var ele = document.getElementById("list");
            ele.appendChild(li);
        }
    }
    //xmlhttp.open("POST","res/Goods/getRecommends.json",true);
    //xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.open("GET", "res/Goods/getRecommends.json?type=" + tp + "&num=20" + "&times=1", false);
    xmlhttp.send();
}
//点击导航栏时，当前文字颜色改变，其他不变
$('.button').on('click', function() {
    $(this).addClass("current").siblings().removeClass("current");
});