package com.hello.service.impl.impl_classes;

import com.hello.service.impl.impl_classes.ShoppingCartDatas.Color;
import com.hello.service.impl.impl_classes.ShoppingCartDatas.Quantity;
import com.hello.service.impl.impl_classes.ShoppingCartDatas.ShoppingCartData;
import com.hello.service.impl.impl_classes.ShoppingCartDatas.Size;

class GoodsId
{
    private long goodsId;

    public long getGoodsId ()
    {
        return goodsId;
    }

    public void setGoodsId (long goodsId)
    {
        this.goodsId = goodsId;
    }
}

public class ShowShoppingCart
{
    private Long buyerId;

    private GoodsId[] goodsList;

    private Quantity[] goodsQuantityList;

    private Color[] goodsColorList;

    private Size[] goodsSizeList;

    public ShowShoppingCart ()
    {

    }

    public ShowShoppingCart (Long buyerId, GoodsId[] goodsList, Quantity[] goodsQuantityList,
                             Color[] goodsColorList, Size[] goodsSizeList)
    {
        this.buyerId = buyerId;
        this.goodsList = goodsList;
        this.goodsQuantityList = goodsQuantityList;
        this.goodsColorList = goodsColorList;
        this.goodsSizeList = goodsSizeList;
    }

    public Long getBuyerId ()
    {
        return buyerId;
    }

    public void setBuyerId (Long buyerId)
    {
        this.buyerId = buyerId;
    }

    public GoodsId[] getGoodsList ()
    {
        return goodsList;
    }

    public void setGoodsList (GoodsId[] goodsList)
    {
        this.goodsList = goodsList;
    }

    public Quantity[] getGoodsQuantityList ()
    {
        return goodsQuantityList;
    }

    public void setGoodsQuantityList (Quantity[] goodsQuantityList)
    {
        this.goodsQuantityList = goodsQuantityList;
    }

    public Color[] getGoodsColorList ()
    {
        return goodsColorList;
    }

    public void setGoodsColorList (Color[] goodsColorList)
    {
        this.goodsColorList = goodsColorList;
    }

    public Size[] getGoodsSizeList ()
    {
        return goodsSizeList;
    }

    public void setGoodsSizeList (Size[] goodsSizeList)
    {
        this.goodsSizeList = goodsSizeList;
    }

    public static GoodsId[] getLongArrFromString (String arr)
    {
        String[] arrString;
        long[] arrLong;
        GoodsId[] result;

        if (arr.compareTo("") != 0)
        {
            arrString = arr.split(" ");
            arrLong = new long[arrString.length];
            for (int index = 0; index < arrLong.length; index++) {
                arrLong[index] = Long.parseLong(arrString[index]);
            }

            result = new GoodsId[arrLong.length];
            for (int index = 0; index < arrLong.length; index++)
                result[index] = new GoodsId();

            for (int index = 0; index < arrLong.length; index++)
                result[index].setGoodsId(arrLong[index]);
        }
        else
        {
            result = new GoodsId[1];
        }
        return result;
    }

    public static ShoppingCartData[] getArrFromString(String source, String type)
    {
        String[] arrString;
        ShoppingCartData[] arr;

        arrString = source.split(" ");
        if (type.compareTo("quantity") == 0)
            arr = Quantity.genArr(arrString.length);
        else if (type.compareTo("color") == 0)
            arr = Color.genArr(arrString.length);
        else
            arr = Size.genArr(arrString.length);

        if (source.compareTo("") != 0)
        {
            for (int index = 0; index < arrString.length; index++)
                arr[index].setValue(arrString[index]);
        }

        return arr;
    }
}
