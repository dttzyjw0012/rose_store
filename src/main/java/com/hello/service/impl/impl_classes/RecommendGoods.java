package com.hello.service.impl.impl_classes;

public class RecommendGoods
{
    private long id;
    private float price;
    private String name;
    private int favorQuantity;
    private int sales;

    public RecommendGoods ()
    {
        this.id = 0;
        this.price = 0;
        this.name = "0";
        this.favorQuantity = 0;
        this.sales = 0;
    }

    public RecommendGoods (long id, float price, String name, int favorQuantity, int sales)
    {
        this.id = id;
        this.price = price;
        this.name = name;
        this.favorQuantity = favorQuantity;
        this.sales = sales;
    }

    public long getId ()
    {
        return id;
    }

    public void setId (long id)
    {
        this.id = id;
    }

    public float getPrice ()
    {
        return price;
    }

    public void setPrice (float price)
    {
        this.price = price;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public int getFavorQuantity ()
    {
        return favorQuantity;
    }

    public void setFavorQuantity (int favorQuantity)
    {
        this.favorQuantity = favorQuantity;
    }

    public int getSales ()
    {
        return sales;
    }

    public void setSales (int sales)
    {
        this.sales = sales;
    }
}