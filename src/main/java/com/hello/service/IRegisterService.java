package com.hello.service;

import com.hello.service.impl.impl_classes.Success;

public interface IRegisterService
{
    Success registerBuyer (String name, String password, String email);
}
