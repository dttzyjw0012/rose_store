package com.hello.service;

import com.hello.service.impl.impl_classes.ShopInfo;

public interface IGetShopInfoService
{
    ShopInfo getShopInfo (Integer id);
}
