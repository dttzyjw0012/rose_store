$(document).ready(function() {
    var current_para = location.search.slice(1);
        var paras = current_para.split("&");
        var tp, get_key;
        for (var index = 0; index < paras.length; index++) {
            var paras1 = paras[index].slice(0, paras[index].indexOf("="));
            if (paras1 === "type") {
                tp = paras[index].slice(paras[index].indexOf("=") + 1);
            }
            if (paras1 === "key") {
                get_key = paras[index].slice(paras[index].indexOf("=") + 1);
            }
        }  
    scroll(tp, get_key); 
})

$(".button").click(function() {
        var current_para = location.search.slice(1);
        var paras = current_para.split("&");
        var tp, get_key;
        for (var index = 0; index < paras.length; index++) {
            var paras1 = paras[index].slice(0, paras[index].indexOf("="));
            if (paras1 === "type") {
                tp = paras[index].slice(paras[index].indexOf("=") + 1);
            }
            if (paras1 === "key") {
                get_key = paras[index].slice(paras[index].indexOf("=") + 1);
            }
        }  
    scroll(tp, get_key);  
});

////type="xp"&num=10&times=0&key=undefined
/*
底部无限滚动事件函数
*/

function scroll(tp, key) {
    // 每次加载添加多少条目
    var itemsPerLoad = 1;
    //申请次数
    var times = 0;
     // 加载flag
    var loading = false;
    // 最多可加载的条目
    var maxItems = 120;
    //获取得到的json数据
    var datas = request_Data(tp, times, key);
    //此处加载文本内容
    //预先加载5条
    addItems(itemsPerLoad, 0, datas);
    // 上次加载的序号
    var lastIndex = 1;
    // 注册'infinite'事件处理函数
    $(document).on('infinite', function() {
        // 如果正在加载，则退出
        if (loading) return;
        // 设置flag
        loading = true;
        // 模拟1s的加载过程
        setTimeout(function() {
            // 重置加载flag
            loading = false;
            if (lastIndex >= maxItems) {
                // 加载完毕，则注销无限加载事件，以防不必要的加载
                $.detachInfiniteScroll($('.infinite-scroll'));
                // 删除加载提示符
                $('.infinite-scroll-preloader').remove();
                return;
            }
            datas = "";
            times++;
            datas = request_Data(tp, times, key);
            // 添加新条目
            addItems(itemsPerLoad, lastIndex, datas);
            // 更新最后加载的序号
            lastIndex = $('.list-container li').length;
            //容器发生改变,如果是js滚动，需要刷新滚动
            $.refreshScroller();
        }, 500);
    });
}

function request_Data(tp, times, keyValue) {
    var xmlhttp;
    var data;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            data = JSON.parse(xmlhttp.responseText);
        }
    }
    //xmlhttp.open("POST","res/Goods/getRecommends.json",true);
        //xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
         //xmlhttp.open("GET","res/goods/getRecommends.json",false);
        xmlhttp.open("GET", "res/goods/getRecommends.json?type=" + tp + "&num=10" + "&times=" + times + "&key=" + keyValue, false);
        xmlhttp.send();
        return data;
}

/*
此处的datas的数量每次应该请求10条，每个li里边都有两个图片

*/
function addItems(number, lastIndex, datas) {
        for (var i = lastIndex + 1; i <= lastIndex + number; i++) {
            var li = document.createElement("li");
            var dv = document.createElement("div");
            dv.setAttribute("class", "row two");
            for (var j = 0; j < 10; j++) {
                var coldv = document.createElement("div");
                coldv.setAttribute("class", "col-50");
                var para = document.createElement("a");
                para.setAttribute("onclick", "change('details.html?id=" + datas[j].id + "')");
                coldv.appendChild(para);
                var inpara = document.createElement("img");
                inpara.setAttribute("src", "images/list/" + j + ".jpg"); //在此处添加要显示的图片编号
                para.appendChild(inpara);
                // para = document.createElement("span");
                // para.setAttribute("class", "sp");
                // node = document.createTextNode("已售" + datas[j].sales + "件");
                // para.appendChild(node);
                // coldv.appendChild(para);
                para = document.createElement("p");
                var node = document.createTextNode(datas[j].name);
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("span");
                para.setAttribute("class", "fl");
                node = document.createTextNode("￥" + datas[j].price);
                para.appendChild(node);
                coldv.appendChild(para);
                para = document.createElement("span");
                para.setAttribute("class", "fr");
                node = document.createTextNode(datas[j].favorQuantity);
                para.appendChild(node);
                coldv.appendChild(para);
                var inpara = document.createElement("span");
                inpara.setAttribute("class", "icon icon-star");
                para.appendChild(inpara);
                dv.appendChild(coldv);
               
            }
            li.appendChild(dv);
            var ele = document.getElementById("list");
            ele.appendChild(li);
            
        }
}