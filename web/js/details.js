//获取商品的id号
var str = location.search;
num = str.indexOf('=') + 1;
var shoppingId = str.substr(num);

$(document).ready(function() {
    loader();
    loader1(); 
    loader2(); 
    loader3();
    loader4();
    loader5();
});

/*
    加载幻灯片里的图片
*/
function loader() {
    for (var i = 0; i < 5; i++) {
        var para = document.createElement("div");
        para.setAttribute("class", "swiper-slide");
        var para1 = document.createElement("img");
        para1.setAttribute("src", "images/" + shoppingId + "/" + (i + 1) + ".webp");
        para1.setAttribute("src", "images/" + (i + 1) + ".webp");
        para1.setAttribute("style", "width:100%;");
        para.appendChild(para1);
        var ele = document.getElementById("slide");
        ele.appendChild(para);
    }
}

/*
    加载商品的基本信息，商品名称、价格、库存...
*/
function loader1() {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            document.getElementById("shoppingname").innerHTML = data.name; //商品名称
            document.getElementById("price").innerHTML = data.price; //商品价格
            document.getElementById("price1").innerHTML = data.price;
            document.getElementById("stock").innerHTML = data.inventoryQuantity; //商品库存
            var color_arr = data.colors.split(" ");
            var size_arr = data.sizes.split(" ");
            for (var i = 0; i < color_arr.length; i++) {
                var para = document.createElement("button");
                para.classList.add("color_button");
                para.setAttribute("onclick", "select(\"color_button\")");
                var txt = document.createTextNode(color_arr[i]);
                para.appendChild(txt);
                var ele = document.getElementById("bt1");
                ele.appendChild(para);
            }
            for (var j = 0; j < size_arr.length; j++) {
                var para = document.createElement("button");
                para.classList.add("size_button");
                para.setAttribute("onclick", "select(\"size_button\")");
                var txt = document.createTextNode(size_arr[j]);
                para.appendChild(txt);
                var ele = document.getElementById("bt2");
                ele.appendChild(para);
            }
        }
    }
    xmlhttp.open("GET", "res/Goods/getInfo.json?id=" + shoppingId, true);
    xmlhttp.send();
}

/*
    加载店铺的信息，店铺名、收藏量，销量...
*/
function loader2() {
    var id = get_Id();
    var xmlhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            document.getElementById("shopname").innerHTML = data.name;
            document.getElementById("sale").innerHTML = data.sumSales;
            document.getElementById("collection").innerHTML = data.favorQuantity;
            document.getElementById("desc").innerHTML = data.descriptionMatchIndex;
            document.getElementById("satisfy").innerHTML = data.qualitySatisfactionIndex;
            var href = document.getElementById("addHref");
            href.setAttribute("href", "store/store.html?id=" + id);
        }
    }
    xmlhttp.open("GET", "res/Shops/getShop.json?id=" + id, true);
    xmlhttp.send();
}

/*
    加载图文详情部分
*/
function loader3() {
    for (var index = 0; index < 7; index++) {
        var para = document.createElement("img");
        para.setAttribute("src", "images/goods/0/list/" + (index + 1) + ".webp");
        var ele = document.getElementById("pic");
        ele.appendChild(para);
    }
}

/*
    加载商品参数部分
*/
function loader4() {
    var para = document.createElement("table");
    for (var index = 0; index < 6; index++) {
        var para1 = document.createElement("tr");
        for (var i = 0; i < 4; i++) {
            var para2 = document.createElement("td");
            para1.appendChild(para2);
        }
        para.appendChild(para1);
    }
    var ele = document.getElementById("data");
    ele.appendChild(para);
}

/*
    加载热卖推荐部分
*/
function loader5() {
    for (var i = 0; i < 4; i++) {
        var para = document.createElement("div");
        para.setAttribute("class", "row");
        for (var j = 0; j < 3; j++) {
            var para1 = document.createElement("div");
            para1.setAttribute("class", "col-33");
            para1.setAttribute("style", "height:245px");
            var img = document.createElement("img");
            img.setAttribute("src", "images/goods/0/tj/1.webp");
            img.setAttribute("style", "width:100%;height:auto");
            para1.appendChild(img);
            var p = document.createElement("p");
            var text = document.createTextNode("数据显示区数据显示区数据显示区数据显示区数据显示区数据显示区");
            p.appendChild(text);
            para1.appendChild(p);
            var span = document.createElement("span");
            var txt = document.createTextNode("数据显示区");
            span.appendChild(txt);
            para1.appendChild(span);
            para.appendChild(para1);
        }
        var ele = document.getElementById("tj");
        ele.appendChild(para);
    }
}

/*
    选择商品的颜色、尺寸页面中，实现商品数量的减操作
*/
$('#reduce').click(function() {
    var data = document.getElementById("num").innerHTML;
    if (data > 0) {
        data--;
        document.getElementById("num").innerHTML = data;
    } else 
        document.getElementById("num").innerHTML = 0;
});

/*
    选择商品的颜色、尺寸页面中，实现商品数量的加操作
*/
$('#add').click(function() {
    var data = document.getElementById("num").innerHTML;
    data++;
    document.getElementById("num").innerHTML = data;
});

/*
    调用时返回店铺的id号
    注:要得到此方法的返回值，需要把ajax改为同步方式请求，否则得不到返回值
*/
function get_Id() {
    var xmlhttp;
    var shop_id;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
            // var shop_id;
            shop_id = data.shopId; //得到店铺的id号
        }
    }
    xmlhttp.open("GET", "res/Goods/getInfo.json?id=" + shoppingId, false);
    xmlhttp.send();
    return shop_id;
}

/*
    选择商品颜色、尺寸页面的显示和隐藏
*/
$('#sele,#select_buy').click(function() {
    $('#view-win').show();
    $('#buy_ok').show();
    $('#sc_ok').hide();
});
$('#select_shopping_cart').click(function() {
    $('#view-win').show();
    $('#sc_ok').show();
    $('#buy_ok').hide();
});
$('#click').click(function() {
    $('#view-win').hide();
});

/*
    点击了"立即购买"按钮之后，在商品的颜色、尺寸页面中点击"确定"时调用
*/
function buy() {
    let post_string = "buyerName=" + getCookie('username');
    post_string += "&goodsId=" + shoppingId;
    post_string += "&color=" + get_select("color_button");
    post_string += "&size=" + get_select("size_button");
    post_string += "&num=" + $('#num').text();
    var xhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {}
    };
    xhttp.open("POST", "res/Goods/buyGoods.json", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(post_string);
}

/*
    点击了"加入购物车"按钮之后，在商品的颜色、尺寸页面中点击"确定"时调用
*/
function add_shopping_cart() {
    let post_string = "buyerName=" + getCookie('username');
    post_string += "&goodsId=" + shoppingId;
    post_string += "&quantity=" + $('#num').text();
    post_string += "&color=" + get_select("color_button");
    post_string += "&size=" + get_select("size_button");
    var xhttp;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {}
    };
    xhttp.open("POST", "res/ShoppingCarts/addShoppingCart.json", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(post_string);
}

function get_select(class_name) {
    let buttons = $('.' + class_name);
    for (let index = 0; index < buttons.length; index++) {
        if (buttons[index].style.color === "red") return buttons[index].innerHTML;
    }
}

function select(class_name) {
    let buttons = $('.' + class_name);
    buttons.css("color", "#3d4145");
    event.target.style.color = "red";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}